Require python 3.6+. Clone the repo and do:

`$ pip install openpyxl`


Download full log CSV from a game on pokernow.club, and pass it as input:

`$ python stats.py path/to/file.csv`


The application will create a new excel summarizing each hand (winner, losers, folders, all-iners, the winning hand, and the amount won.

