import re
import sys
import json
import os
import time
from pprint import pprint
from collections import Counter
from openpyxl import load_workbook
from openpyxl import Workbook
from openpyxl.utils import get_column_letter
from openpyxl.chart import AreaChart, Reference
from openpyxl.styles import Alignment
from os import startfile
from tqdm import tqdm

"""
Some weaknesses:
- If players put a @ in their name, this will break

"""

# Regex
# "-- ending hand #129
END_HAND = re.compile(r'^"--\sending\shand\s#(?P<handnum>\d+)\s')

# "-- starting hand #128
START_HAND = re.compile(r'^"--\sstarting\shand\s#(?P<handnum>\d+)\s')

# """soochnik @ LoUyubCBIK"" collected 1440 from pot with Two Pair, 10's & 9's (combination: 10♦, 10♥, 9♠, 9♥, J♣)",2020-10-24T04:27:12.984Z,160351363298600
# """JuMBiE @ eanCw1BAVl"" collected 180 from pot",2020-10-24T04:43:57.621Z,160351463762201
WINNER_OPT_HAND = re.compile(r'^"""(?P<friendly>[^@]+)\s@\s(?P<pid>[^"]+)""\scollected\s(?P<winnings>\d+)\sfrom\spot(\swith\s(?P<winninghand>[^,]+))?')

# """Jamesbon! @ 850djxlnD7"" folds",2020-10-24T04:27:00.060Z,160351362006100
FOLDS = re.compile(r'^"""(?P<friendly>[^@]+)\s@\s(?P<pid>[^"]+)""\sfolds')
PLAYERS = re.compile(r'^"""(?P<friendly>[^@]+)\s@\s(?P<pid>[^"]+)""\s')

# """Vikstar @ WTqx_PLkYA"" raises to 600 and go all in",2020-10-24T04:26:42.776Z,160351360277600
ALL_IN = re.compile(r'^"""(?P<friendly>[^@]+)\s@\s(?P<pid>[^"]+)"".*?and\sgo\sall\sin')

# "river: 9♠, 9♥, 10♥, 3♠ [7♥]",2020-10-24T04:27:09.957Z,160351362996600
# "turn: 9♠, 9♥, 10♥ [3♠]",2020-10-24T04:27:06.927Z,160351362692900
# "flop:  [9♠, 9♥, 10♥]",2020-10-24T04:27:03.913Z,160351362391400
RIVER = re.compile(r'^"river:')
TURN = re.compile(r'^"turn:')
FLOP = re.compile(r'^"flop:')

# """soochnik @ LoUyubCBIK"" raises to 4062 and go all in",2020-10-24T04:36:21.054Z,160351418105500
# """Meeeshuu @ DLnYNf5MP1"" bets 80",2020-10-24T04:36:17.806Z,160351417780700
RAISER = re.compile(r'^"""(?P<friendly>[^@]+)\s@\s(?P<pid>[^"]+)""\sraises\sto\s(?P<raise>\d+)')
BETTER = re.compile(r'^"""(?P<friendly>[^@]+)\s@\s(?P<pid>[^"]+)""\sbets\s(?P<bet>\d+)')


# "Player stacks: #1 ""Meeeshuu @ DLnYNf5MP1"" (2803) | #2 ""soochnik @ LoUyubCBIK"" (6580) | #4 ""Jamesbon! @ 850djxlnD7"" (2017) | #6 ""Vikstar @ WTqx_PLkYA"" (600) | #9 ""JuMBiE @ eanCw1BAVl"" (1000)",2020-10-24T04:26:32.040Z,160351359204101
# Will split by | and then deal with each
STACKS_LINE = re.compile(r'^"Player\sstacks:')
PLAYER_CHIPS = re.compile(r'.*""(?P<friendly>[^@]+)\s@\s(?P<pid>[^"]+)""\s\((?P<chips>\d+)')


def main():
    """
    Input: Full log from a match at pokernow.club

    Output: Summary of Wins per scenario in Excel.

    TODO: Summary of Hands (winner/loser/folders/all-ins/segement), Chart of Wealth Changes, and a JSON of stats

    """
    print("Poker Stats")

    with open(input_file, "r", encoding="utf-8") as f:
        lines = [l.strip().split(",") for l in f.readlines()]
        lines = ["".join(line[:-2]) for line in lines]

    hands, extra = parse_log(lines)
    rows, player_info = collect_information(hands)
    player_stats = collect_stats(rows, player_info)
    save_player_stats(player_stats)
    write_excel(rows, player_info, player_stats)


def save_player_stats(player_stats: dict) -> None:
    with open(get_output_filename("json"), "w", encoding="utf=8") as f:
        json.dump(player_stats, f, ensure_ascii=False, indent=2, sort_keys=True)


def collect_stats(rows: list, p_info: dict) -> dict:
    """
    Return a dictionary with some interesting information per player:

    {
        "Meesh": {
            "Total wins": 10,
            "Players won against": {
                "Gib": 3,
                "Mo": 2
            },
            "Winning Segment": {
                "River": 5,
                "Turn": 6,
                "Flop": 3,
                "Preflop": 3
            },
            "Total chips won": 5000,
            "All-in count": 5
        }
    }
    """

    result = dict()

    # Prefill the result 
    for row in rows:
        winners = [p_info[x].most_common(1)[0][0] for x in row[1]] #winner
        all_in = [p_info[x].most_common(1)[0][0] for x in row[4]] #all_in
        people = winners + all_in
        for person in people:
            if person not in result:
                result[person] = dict()
                result[person]["Players won against"] = dict()
                result[person]["Winning segment"] = dict()
                result[person]["Total chips won"] = 0
                result[person]["All-in count"] = 0
                result[person]["Hands played"] = 0
                result[person]["Action - Bets"] = { 'count': 0, 'total': 0 }
                result[person]["Action - Raises"] = { 'count': 0, 'total': 0 }


    # Main processing
    for row in rows:
        winners = [p_info[x].most_common(1)[0][0] for x in row[1]] #winner
        losers = [p_info[x].most_common(1)[0][0] for x in row[2]] #losers
        folders = [p_info[x].most_common(1)[0][0] for x in row[3]] #folders
        all_in = [p_info[x].most_common(1)[0][0] for x in row[4]] #all_in
        winning_hand = str(row[5]) # winning hand
        amount = int(row[6]) # amount
        segment = str(row[7]) # segment
        b = row[9] #betters
        r = row[10] #raisers

        for k, v in b.items():
            person = p_info[k].most_common(1)[0][0]
            result[person]['Action - Bets']['count'] += v['count']
            result[person]['Action - Bets']['total'] += v['total']

        for k, v in r.items():
            person = p_info[k].most_common(1)[0][0]
            result[person]['Action - Raises']['count'] += v['count']
            result[person]['Action - Raises']['total'] += v['total']

        in_hand = winners + losers + folders 
        for person in in_hand:
            result[person]["Hands played"] += 1


        for winner in winners:
            if len(losers) == 0:
                losers.append("_Uncontested")

            for loser in losers:
                if loser not in result[winner]["Players won against"]:
                    result[winner]["Players won against"][loser] = 0    
                result[winner]["Players won against"][loser] += 1

            for player in all_in:
                result[player]["All-in count"] += 1

            if segment not in result[winner]["Winning segment"]:
                result[winner]["Winning segment"][segment] = 0
            result[winner]["Winning segment"][segment] += 1

            result[winner]["Total chips won"] += amount

    return result


def write_excel(rows: list, player_info: dict, player_stats: dict) -> None:
    wb = Workbook()
    sheet = wb.worksheets[0]
    sheet.title = "Hands"
    headings = ["Hand Number", "Winner(s)", "Loser(s)", "Folders", "Anybody All In?", "Winning Hand", "Amount", "Segment"]
    sheet.append(headings)
    chips = list()

    for row in rows:
        excel_row = list()
        #r = [str(x) for x in row]
        excel_row.append(int(row[0]))
        excel_row.append(get_friendly(row[1], player_info)) #winner
        excel_row.append(get_friendly(row[2], player_info)) #losers
        excel_row.append(get_friendly(row[3], player_info)) #folders
        excel_row.append(get_friendly(row[4], player_info)) #all_in
        excel_row.append(str(row[5])) # winning hand
        excel_row.append(int(row[6])) # amount
        excel_row.append(str(row[7])) # segment
        #excel_row.append(get_friendly(row[7], player_info)) #all_players
        chips.append(row[11])

        sheet.append(excel_row)

    adjust_sheet_columns(sheet)
    sheet.auto_filter.ref = sheet.dimensions

    # Deal with the players hand values
    chips_rows, sorted_player_headings = convert_chips_rows(chips, player_info)
    sheet = wb.create_sheet('Charts')
    sheet.append(sorted_player_headings)
    for row in chips_rows:
        sheet.append(row)

    chart = AreaChart(grouping="stacked")
    chart.title = "Change of Wealth"
    chart.x_axis.title = "Hand Number"
    chart.y_axis.title = "Chips"
    chart.legend.position = "b"
    chart.width = 25
    chart.height= 20

    data = Reference(sheet, min_col=1, min_row=1, max_col=len(sorted_player_headings), max_row=len(chips_rows))
    chart.add_data(data, titles_from_data=True)
    sheet.add_chart(chart, "H2")

    adjust_sheet_columns(sheet)
    sheet.auto_filter.ref = sheet.dimensions

    add_stats_sheet(wb.create_sheet('Stats'), player_stats)

    o = get_output_filename()
    wb.save(o)
    #time.sleep(1)
    startfile(os.path.normpath(o))


def add_stats_sheet(sheet, player_stats: dict) -> None:
    k = list(player_stats.keys())
    keys = player_stats[k[0]].keys()
    columns = list(['Players'] + sorted(list(keys)))

    sheet.append(columns)
    # Loop over each player
    for k,v in player_stats.items():
        row = list()
        row.append(k)
        for k1 in columns[1:]:
            val = v[k1]
            if type(val) == int:
                row.append(val)
            elif type(val) == dict:
                inner_keys = sorted(val.keys())
                lines = list()
                for ik in inner_keys:
                    lines.append(f"{ik}: {val[ik]}")

                row.append("\n".join(lines))

        sheet.append(row)

    for r in sheet:
        for cell in r:
            cell.alignment = Alignment(wrapText=True, vertical='center', horizontal='center')

    adjust_sheet_columns(sheet)
    sheet.auto_filter.ref = sheet.dimensions


def convert_chips_rows(chips: list, player_info: dict) -> (list, list):
    players = set()
    for d in chips:
        for k in d.keys():
            players.add(k)

    players = list(players)
    players = sorted(players, key=lambda x: get_friendly([x], player_info).lower())

    result = list()
    for d in chips[::-1]:
        row = list()
        for p in players:
            if p in d:
                row.append(d[p])
            else:
                row.append("")
        result.append(row)

    return result, [get_friendly([x], player_info) for x in players]


def get_output_filename(extension="xlsx") -> str:
    tail = input_file.split("/")[-1]
    name = "".join(tail.split(".")[:-1])
    return f"output/result_{name}.{extension}"


def adjust_sheet_columns(working_sheet):
    column_widths = []
    for row in working_sheet:
        for i, cell in enumerate(row):
            if cell is not None and cell.value is not None:
                if len(column_widths) > i:
                    if len(str(cell.value)) > column_widths[i]:
                        column_widths[i] = len(str(cell.value))
                else:
                    column_widths += [len(str(cell.value))]
            else:
                column_widths.append(4)  # default width?

    for i, column_width in enumerate(column_widths):
        working_sheet.column_dimensions[get_column_letter(i + 1)].width = column_width + 2


def get_friendly(p: list, p_info: dict) -> str:
    result = list()
    for entry in p:
        t = p_info[entry].most_common(1)[0]
        result.append(t[0])

    return str(sorted(result, key=lambda v: v.upper()))


def collect_information(hands: list) -> (list, dict):
    player_lookup = dict()
    rows = list()

    for hand in hands:
        row = list()
        handnum, winners, losers, folders, all_in, winning_hand, amount, players, segment, betters, raisers, hand_chips = parse_hand(hand)
        row.append(handnum)
        row.append(winners)
        row.append(losers)
        row.append(folders)
        row.append(all_in)
        row.append(winning_hand)
        row.append(amount)
        row.append(segment)
        row.append(players)
        row.append(betters)
        row.append(raisers)
        row.append(hand_chips)
        rows.append(row)

        #debug:
        #if len(all_in) > 1:
        #    print(f"{handnum} - Winners {winners}; Losers: {losers}; Folders: {folders}; All in: {all_in}, Winning Hand: {winning_hand}; Amount: {amount}; Players: {players.keys()}")

        for k, v in players.items():
            if k in player_lookup: 
                player_lookup[k].update([v])
            else:
                player_lookup[k] = Counter()

    return rows, player_lookup


def parse_hand(hand: list) -> (int, list, str, str, str, dict, str, dict, dict, dict):
    # "-- ending hand #112 --",2020-10-24T04:27:12.984Z,160351363298601
    # """soochnik @ LoUyubCBIK"" collected 1440 from pot with Two Pair, 10's & 9's (combination: 10♦, 10♥, 9♠, 9♥, J♣)",2020-10-24T04:27:12.984Z,160351363298600
    # "river: 9♠, 9♥, 10♥, 3♠ [7♥]",2020-10-24T04:27:09.957Z,160351362996600
    # "turn: 9♠, 9♥, 10♥ [3♠]",2020-10-24T04:27:06.927Z,160351362692900
    # "flop:  [9♠, 9♥, 10♥]",2020-10-24T04:27:03.913Z,160351362391400
    # """Vikstar @ WTqx_PLkYA"" shows a 6♠, 6♣.",2020-10-24T04:27:00.891Z,160351362089201
    # """soochnik @ LoUyubCBIK"" shows a 10♦, J♣.",2020-10-24T04:27:00.891Z,160351362089200
    # """Jamesbon! @ 850djxlnD7"" folds",2020-10-24T04:27:00.060Z,160351362006100
    # """soochnik @ LoUyubCBIK"" calls 600",2020-10-24T04:26:47.925Z,160351360792600
    # """JuMBiE @ eanCw1BAVl"" folds",2020-10-24T04:26:44.286Z,160351360428700
    # """Vikstar @ WTqx_PLkYA"" raises to 600 and go all in",2020-10-24T04:26:42.776Z,160351360277600
    # """Jamesbon! @ 850djxlnD7"" calls 160",2020-10-24T04:26:38.446Z,160351359844700
    # """soochnik @ LoUyubCBIK"" raises to 160",2020-10-24T04:26:35.874Z,160351359587600
    # """Meeeshuu @ DLnYNf5MP1"" folds",2020-10-24T04:26:33.577Z,160351359357800
    # """JuMBiE @ eanCw1BAVl"" posts a big blind of 80",2020-10-24T04:26:32.040Z,160351359204108
    # """Vikstar @ WTqx_PLkYA"" posts a small blind of 40",2020-10-24T04:26:32.040Z,160351359204107
    # "Player stacks: #1 ""Meeeshuu @ DLnYNf5MP1"" (2803) | #2 ""soochnik @ LoUyubCBIK"" (6580) | #4 ""Jamesbon! @ 850djxlnD7"" (2017) | #6 ""Vikstar @ WTqx_PLkYA"" (600) | #9 ""JuMBiE @ eanCw1BAVl"" (1000)",2020-10-24T04:26:32.040Z,160351359204101
    # "-- starting hand #112  (No Limit Texas Hold'em) (dealer: ""Jamesbon! @ 850djxlnD7"") --",2020-10-24T04:26:32.040Z,160351359204100

    handnum = ""
    winner = ""
    winners = set()
    amount = ""
    winning_hand = ""
    river_seen, turn_seen, flop_seen = False, False, False
    segment = "0 - Preflop"
    all_in = set()
    losers = set()
    folders = set()
    players = dict()
    betters = dict()
    raisers = dict()
    hand_chips = dict()

    for line in hand:
        result = END_HAND.match(line)
        if result:
            handnum = result.group('handnum')

        result = PLAYERS.match(line)
        if result:
            players[result.group('pid')] = result.group('friendly').strip()

        result = WINNER_OPT_HAND.match(line)
        if result:
            winner = result.group('pid')
            amount = result.group('winnings')
            winning_hand = result.group('winninghand')
            winners.add(winner)

        result = FOLDS.match(line)
        if result:
            folders.add(result.group('pid'))

        result = ALL_IN.match(line)
        if result:
            all_in.add(result.group('pid'))

        result = BETTER.match(line)
        if result:
            handle_bet_raise(result, betters, 'bet')

        result = RAISER.match(line)
        if result:
            handle_bet_raise(result, raisers, 'raise')

        result = RIVER.match(line)
        if result:
            river_seen = True

        result = TURN.match(line)
        if result:
            turn_seen = True

        result = FLOP.match(line)
        if result:
            flop_seen = True

        result = STACKS_LINE.match(line)
        if result:
            stacks = line.split("|")
            for s in stacks:
                rs = PLAYER_CHIPS.match(s)
                if rs:
                    pid = rs.group('pid')
                    chips = rs.group('chips')
                    hand_chips[pid] = int(chips)


    if river_seen:
        segment = "3 - River"
    elif turn_seen:
        segment = "2 - Turn"
    elif flop_seen:
        segment = "1 - Flop"

    
    # The losers are inferred: all players minus the winner minus the folders
    # Whoever is left, lost on cards in the showdown!
    p = set(players.keys())
    p = p - set(winners)
    if folders:
        p = p - set(folders)
    losers = list(p)

    #print(winners, losers, folders, players)
    if len(winners) + len(losers) + len(folders) != len(players.keys()):
        raise Exception("Something awful happened", handnum)

    return handnum, list(winners), list(losers), list(folders), list(all_in), winning_hand, amount, players, segment, betters, raisers, hand_chips


def handle_bet_raise(result_match, result_dict, match_key) -> None:
    person = result_match.group('pid')
    amount = result_match.group(match_key)

    if person not in result_dict:
        result_dict[person] = { 'count': 0, 'total': 0 }

    result_dict[person]['count'] += 1
    result_dict[person]['total'] += int(amount)


def parse_log(lines: list) -> (list, list):
    hands = list()
    extra = list()

    in_hand = False
    hand_lines = list()
    for line in tqdm(lines):

        if in_hand:
            #print(f"In hand line: {line}")
            result = START_HAND.match(line)
            if result:
                """
                We are at the start of the hand. Time to clean up
                """
                #print(f"Game start line detected, hand: {result.group('handnum')}")
                in_hand = False
                hand_lines.append(line)
                hands.append(hand_lines)
                hand_lines = list()
           
            else:
                hand_lines.append(line)
                continue

        else:
            result = END_HAND.match(line)
            if result:
                """
                This signifies that we're starting a new hand. Every line after 
                we should collect as part of the hand
                """
                #print(f"Game ended line detected, hand: {result.group('handnum')}")
                in_hand = True
                hand_lines.append(line)
            
            else:
                extra.append(line)
    return hands, extra


if __name__ == "__main__":
    os.makedirs("output", exist_ok=True)
    input_file = sys.argv[1]
    print(input_file)
    main()